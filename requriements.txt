absl-py @ file:///opt/concourse/worker/volumes/live/038c7640-529d-45f3-464f-ee37f5e0f4eb/volume/absl-py_1623867246969/work
astor==0.8.1
astunparse==1.6.3
cached-property @ file:///tmp/build/80754af9/cached-property_1600785575025/work
cachetools==4.2.2
certifi==2021.5.30
charset-normalizer==2.0.4
coverage @ file:///opt/concourse/worker/volumes/live/4f1a5e30-f675-4496-6570-bc6669a34f2a/volume/coverage_1614614860186/work
Cython @ file:///opt/concourse/worker/volumes/live/432fe198-1725-458b-4530-7523a5e3de1a/volume/cython_1626256963932/work
flatbuffers==1.12
gast==0.4.0
google-auth==1.34.0
google-auth-oauthlib==0.4.5
google-pasta==0.2.0
grpcio==1.34.1
h5py==3.1.0
idna==3.2
importlib-metadata @ file:///opt/concourse/worker/volumes/live/4e25a0be-45cc-4b73-4314-4604f00e30a4/volume/importlib-metadata_1617877365451/work
keras==2.5.0rc0
Keras-Applications @ file:///tmp/build/80754af9/keras-applications_1594366238411/work
keras-nightly==2.5.0.dev2021032900
Keras-Preprocessing @ file:///tmp/build/80754af9/keras-preprocessing_1612283640596/work
Markdown @ file:///opt/concourse/worker/volumes/live/255f8945-2411-4469-7c82-a749af3aef33/volume/markdown_1614363822772/work
mkl-fft==1.3.0
mkl-random @ file:///opt/concourse/worker/volumes/live/133f1d0b-8fd8-4fa0-679e-7214401cbd75/volume/mkl_random_1626186083386/work
mkl-service==2.4.0
numpy==1.19.5
oauthlib==3.1.1
opencv-python==4.5.3.56
opt-einsum @ file:///tmp/build/80754af9/opt_einsum_1621500238896/work
Pillow==8.3.1
protobuf==3.17.2
pyasn1==0.4.8
pyasn1-modules==0.2.8
PyYAML==5.4.1
requests==2.26.0
requests-oauthlib==1.3.0
rsa==4.7.2
scipy @ file:///opt/concourse/worker/volumes/live/a038d231-fbb5-4cf5-6b71-513b1c94ddce/volume/scipy_1618855950518/work
six==1.15.0
tensorboard==2.6.0
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.8.0
tensorflow==2.5.0
tensorflow-estimator==2.5.0
termcolor==1.1.0
typing-extensions==3.7.4.3
urllib3==1.26.6
Werkzeug @ file:///home/ktietz/src/ci/werkzeug_1611932622770/work
wrapt==1.12.1
zipp @ file:///tmp/build/80754af9/zipp_1625570634446/work
