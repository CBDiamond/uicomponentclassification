# UI Automation and Testing
UI testing/Automation project is a computer vision-based object detection approach which uses image detection and classification techniques to generate a JSON file representation of a given screenshot image.
It takes screenshot or UI mock-ups as an input image and generates a JSON file which contains structure of all UI components so a custome parser can be implemented to generate UI wireframes. Also, by creating a custom parser two images can be compared for similarity simply by using the key-value pairs of two generated JSON files.

# Trained Model
Our classifier is a deep learning based model which is trained on ReDraw dataset. The trained model can be found inside `/model` directory. We also provide the flexibility to provide any other trained model to our code in `.h5` format.

# Dependency
- Tensforflow==2.5
- Keras==2.5
- opencv-python==4.5.3.56

The entire list of dependencies can be installed using `requirements.txt` file

# Project Background
The below image depicts the entire project workflow in a brief.
![](images/workflow.png)

# Generating an Annotated Image
For generating a JSON file follow the below steps
1. Download the repository
2. Execute the program using `python3 __init__.py --image <path-to-image-file>`
3. The `__init__.py` takes two arguments 
    - --image: Path to the screenshot for which JSON file is required
    - --model: Path to classifier. `default: model/redraw-cnn.h5`
Execute command `python3 __init__.py -h` to view the list of options available.
4. The output of the file is a annoted image with the detected and classified UI components.

# Training a new Model
The training script can be found at `notebooks/train_redraw.ipynb`. Training uses `ImageDataGenerator` to create validation and training datasets. To train on a new set of images the same script can be used just by chaning the images in the folder structure. Below code show how to prepare validation and training dataset
```python
train_dataset = ImageDataGenerator(rescale=1/255).flow_from_directory('/content/dataset/train', 
                    target_size=(64,64),
                    batch_size=5,
                    class_mode='categorical')
validation_dataset = ImageDataGenerator(rescale=1/255).flow_from_directory('/content/dataset/validation', 
                    target_size=(64,64),
                    batch_size=5,
                    class_mode='categorical')
```                    
The below image shows the folder structure required ![](images/dir_structure.png). The component folders should contain the GUI component images and the name should be the required classnames.








