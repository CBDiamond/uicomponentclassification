import cv2 as cv
import numpy as np
import os
import shutil

class ContourDetector():
    def __init__(self, img_path):
        self.img = cv.imread(img_path) 
        self.temp_img = self.img.copy()
        self.idx = 0
        self.crd = []
        self.padding = 8
        self.clipped_imgs = []

        # Creating the output directory
        if not os.path.exists('../output'):
            os.makedirs('../output')     
        else:
            shutil.rmtree('../output')  
            os.makedirs('../output')     

    def get_gray_img(self):
        return cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)

    def get_blurred_img(self):
        return cv.medianBlur(self.get_gray_img(),9)

    def get_edges(self):
        return cv.Canny(self.get_blurred_img(),127, 255)

    def apply_thresholding(self):
        return cv.adaptiveThreshold(self.get_edges(),255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV,15,3)        

    def apply_morphology(self):
        # Structuring Element
        kernel = cv.getStructuringElement(cv.MORPH_RECT, (3,3))
        # Close Morphology
        close = cv.morphologyEx(self.apply_thresholding(), cv.MORPH_CLOSE, kernel, iterations=1)
        # Dilation
        dilate = cv.dilate(close, kernel, iterations=1)

        return dilate

    def get_contours(self):
        # Countor Finding
        dilate = self.apply_morphology()
        contours, hier = cv.findContours(dilate, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        #print('Countours Found {0}'.format(len(contours)))
        return (contours, hier)

    def draw_and_clip_contours(self):
        contours, _ = self.get_contours()
        for cnt in contours:
            # Calculate Contour Size
            x,y,w,h = cv.boundingRect(cnt)
            
            # Logic to remove single character unwanted contours
            if h > 15 and w > 15:
                # Finding Coordinates
                M = cv.moments(cnt)
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                crdt = tuple([cX,cY,x,y,w,h])
                self.crd.append(crdt)
                sample_img = self.img[y-self.padding:y+h+self.padding, x-self.padding:x+w+self.padding]
                # Write the detected contours
                self.clipped_imgs.append('../output/img_'+str(self.idx)+'.png')
                cv.imwrite('../output/img_'+str(self.idx)+'.png', sample_img)
                self.idx +=1

    def get_cliped_images(self):
        self.draw_and_clip_contours()
        return self.clipped_imgs

    def get_coordinates(self):
        return self.crd

    def get_annotated_img(self, class_names):
        # Write Coordinate on Image
        
        for idx, crdt in enumerate(self.crd):
            cX, cY, x, y, w, h =  crdt
            crdt_output = cv.rectangle(self.temp_img, (x,y), (x+w, y+h), (255,0,0))                
            #cv.circle(crdt_output, (cX, cY), 7, (255, 255, 255), -1)         
            #cX, cY, _, _, _, _ =  crdt
            if idx < len(class_names):
                cv.putText(crdt_output,class_names[idx],(cX - 20, cY - 20),cv.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 2)

        return crdt_output

        
